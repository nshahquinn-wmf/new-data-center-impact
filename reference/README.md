## data_center_switches ##
Data on cases (currently only 2018 to present) where areas were switched from one data center to another. Short-term switches (where the new routing was maintained for less than 6 months) are not included. This data was collected from the Git history of the [geo-maps file in the operations/dns repository](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/dns/+/refs/heads/master/geo-maps).

For ease of analysis, the dataset also groups the switches into cohorts of countries whiched switched to the same new data center around the same time.
