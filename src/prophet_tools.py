from copy import deepcopy
from itertools import cycle, islice

import numpy as np
import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as pgo
from prophet import Prophet

from data_access import get_unique_devices, get_page_views


def get_group_data(variable_name, groups):
    """
    groups should be a dict describing the groups as follows:
    
    ```
    "countries switching in 2018-07": {
        "countries": ['TH', 'TL', 'NR', 'MM', 'TO'],
        "switch time": "2018-07-01"
    },
    "India": {
        "countries": ["IN"],
        "switch time": "2018-03-01"
    }
    ```
    
    The variable name should be "unique_devices" or "page_views"
    """
    output = deepcopy(groups)
    

    for name, group in output.items():
        countries = group["countries"]
        
        if variable_name == "unique_devices":
            data = get_unique_devices(countries)
        elif variable_name == "page_views":
            data = get_page_views(countries)
        else:
            raise ValueError("The variable name should be `unique_devices` or `page_views`.")
        
        data["switch"] = False
        data.loc[group["switch time"], "switch"] = True
        
        
        group["data"] = data.reset_index()
    
    return output


def model_trend(data, variable_name):    
    data = (
        data
        [["ds", variable_name]]
        .rename({variable_name: "y"}, axis="columns")
    )
        
    model = Prophet(
        # Without this, Prophet only considers changepoints in the first 80% of the data, which
        # would not cover the Marseille switch
        changepoint_range=0.92,
        # Increasing the aggressiveness in finding changepoints
        changepoint_prior_scale=0.1
    )
    
    model.fit(data)
    
    # We don't actually want a forecast, but Prophet won't work without one
    forecast = model.predict(model.make_future_dataframe(periods=1, freq="MS"))
    
    significant_changepoints = pd.DataFrame({
        "ds": model.changepoints[
            np.abs(np.nanmean(model.params['delta'], axis=0)) >= 0.01
        ],
        "changepoint": True
    })
    
    modeled_data = (
        forecast[["ds", "trend"]]
        .merge(significant_changepoints, how="left")
        .fillna({"changepoint": False})
        .assign(trend=lambda df: df["trend"].astype(int))
    )
    
    return model, modeled_data

def model_groups_trend(variable_name, groups):
    for _, group in groups.items():
        model, trend = model_trend(group["data"], variable_name)
        group["model"] = model
        group["trend"] = trend

def plot_groups_with_trend(groups, variable_name, new_datacenter):
    n_groups = len(groups)
    vivid = px.colors.qualitative.Vivid
    color_sequence = list(islice(cycle(vivid), n_groups))
    height = 500 * n_groups
    vertical_spacing = 50 / height
    
    fig = plotly.subplots.make_subplots(
        rows=n_groups,
        cols=1,
        subplot_titles=[n for n in groups],
        vertical_spacing=vertical_spacing
    )
    
    for i, (name, group) in enumerate(groups.items()):
        data = group["data"]
        trend = group["trend"]
        switch_time = pd.Timestamp(group["switch time"])
        row = i + 1
        color = color_sequence[i]
        
        # Add the actual data
        fig.append_trace(
            pgo.Scatter(
                x=data["ds"],
                y=data[variable_name],
                name="actual",
                line_width=1.5,
                line_color=color,
                legendgroup=row
            ),
            row=row,
            col=1
        )
        
        # Add the modeled trend
        fig.append_trace(
            pgo.Scatter(
                x=trend["ds"],
                y=trend["trend"],
                name="Modeled trend",
                line_color=color,
                line_width=3,
                legendgroup=row
            ),
            row=row,
            col=1
        )

        changepoints = trend.query("changepoint == True")
        fig.append_trace(
            pgo.Scatter(
                x=changepoints["ds"],
                y=changepoints["trend"],
                legendgroup=row,
                name="Modeled trend changes",
                marker_symbol="circle",
                marker_color=color,
                marker_opacity=0.75,
                marker_size=15,
                mode="markers"
            ),
            row=row,
            col=1
        )
        
        fig.add_vline(
            # https://github.com/plotly/plotly.py/issues/3065
            x=switch_time.timestamp() * 1000,
            line_color=color,
            line_width=2,
            line_dash="dash",
            row=row,
            annotation_text="data<br> center<br> switch",
            annotation_position="top right"
        )
        
    
    if variable_name == "unique_devices":
        y_axis_title = "monthly unique devices"
    elif variable_name == "page_views":
        y_axis_title = "monthly page views"
        
    variable_name = variable_name.replace("_", " ")
    
    fig.update_yaxes(title_text=y_axis_title)
    
    fig.update_layout(
        title_text=f"{variable_name} in different groups switching to the {new_datacenter} data center",
        height=height,
        width=800,
        # https://www.kaggle.com/code/jrmistry/plotly-how-to-make-individual-legends-in-subplot/notebook
        legend_tracegroupgap=450
    )
    
    return fig
