import pandas as pd

import wmfdata as wmf


LATEST_YEAR = 2023
LATEST_MONTH = 8


wikis = wmf.presto.run("""
    SELECT *
    FROM canonical_data.wikis
""")

countries = wmf.presto.run("""
    SELECT *
    FROM canonical_data.countries
""")

# Set up to correct the unique devices overcount
overcount_estimates = (
    pd
    .read_csv("../reference/2022-08-14_unique_devices_overcount_estimates.tsv", sep="\t")
    [["country_code", "overcount_rate"]]
    .set_index("country_code")
)

# Duplicating a country's row for each month of the overcount allows us to apply the correction 
# using joins rather than pd.apply, for a potentially significant performance improvement
overcount_dates = pd.date_range(start="2021-02-01", end="2022-07-01", freq="MS")
new_index = pd.MultiIndex.from_product(
    [overcount_estimates.index, overcount_dates],
    names=["country", "ds"]
)

overcount_estimates = overcount_estimates.reindex(new_index, level=0)


def get_unique_devices_per_project_family(countries, aggregate_countries=True):
    # Get the per-country data
    countries = wmf.utils.sql_tuple(countries)
    
    ud = (
        wmf.presto.run(f"""
            SELECT
                country_code AS country,
                CONCAT(CAST(year AS VARCHAR), '-', LPAD(CAST(month AS VARCHAR), 2, '0'), '-01') AS ds,
                uniques_estimate AS unique_devices
            FROM wmf.unique_devices_per_project_family_monthly
            WHERE
                country_code IN {countries}
                AND project_family = 'wikipedia'
                AND year > 0
            ORDER BY
                country,
                ds
        """)
        .assign(
            ds=lambda df: pd.to_datetime(df["ds"])
        )
        .set_index(["country", "ds"])
    )
    
    # Correct using the per-country overcount rate
    ud = (
        ud
        .merge(overcount_estimates, how="left", left_index=True, right_index=True)
        .fillna(0)
    )
    
    ud["unique_devices"] = (ud["unique_devices"] / (1 + ud["overcount_rate"])).astype(int)
    
    ud = ud.drop("overcount_rate", axis="columns")
    
    # Sum countries into a single series if requested
    if aggregate_countries:
        ud = ud.groupby("ds").apply("sum")
    
    return ud


def get_unique_devices(countries, aggregate_countries=True):
    return get_unique_devices_per_project_family(countries, aggregate_countries=aggregate_countries)


def get_page_views(countries, aggregate_countries=True):
    # Get the per-country data
    countries = wmf.utils.sql_tuple(countries)
    
    query = f"""
        SELECT
            country_code AS country,
            CONCAT(year, '-', LPAD(month, 2, '0'), '-01') AS ds,
            SUM(view_count) AS page_views
        FROM wmf.projectview_hourly
        WHERE
            country_code IN {countries}
            AND agent_type != 'spider'
            AND (
                year < {LATEST_YEAR}
                OR year = {LATEST_YEAR} and month <= {LATEST_MONTH}
            )
        GROUP BY
            country_code,
            year,
            month
        ORDER BY
            country,
            ds
    """
    # We need Spark since the pageview data is much larger than the unique devices data
    pv = wmf.spark.run(query)
    
    pv = (
        pv
        .assign(ds=lambda df: pd.to_datetime(df["ds"]))
        .set_index(["country", "ds"])
    )
    
    if aggregate_countries:
        pv = pv.groupby("ds").apply("sum")
    
    return pv


def get_unique_devices_per_domain(country, domain):
    mobile_domain = (
        wikis
        .query("domain_name == @domain")
        ["mobile_domain_name"]
        .iloc[0]
    )
    
    query = f"""
        SELECT
            CONCAT(
                CAST(year AS VARCHAR),
                '-',
                LPAD(CAST(month AS VARCHAR), 2, '0'),
                '-01'
            ) AS time,
            SUM(uniques_underestimate) AS uniques_underestimate
        FROM wmf.unique_devices_per_domain_monthly
        WHERE
            country_code = '{country}'
            AND domain IN ('{domain}', '{mobile_domain}')
            AND year > 0
        GROUP BY
            year,
            month
        ORDER BY
            time
    """
    
    data = (
        wmf.presto.run(query)
        .assign(time=lambda df: pd.to_datetime(df["time"]))
    )
    
    return data
